#!/usr/bin/env python3
#
# Utility to plot boxplot as ASCII in the terminal. Expects a list of numbers
# and will make a boxplot out of it.
#

import numpy as np
import sys
import math
import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
    "--input", type=str, help="File containing the data, default is read from stdin."
)
parser.add_argument(
    "--nticks", type=int, default=5, help="Number of ticks on the x-axis"
)
parser.add_argument(
    "--maxima", action="store_true", help="Width of the plot is the same as data"
)
parser.add_argument(
    "--outliers", action="store_true", help="Plot outliers (1% and 99%)"
)
parser.add_argument("--debug", action="store_true")


def read_data(f):
    data = []
    for line in f:
        if not line:
            continue
        data.append(line.strip())

    return data


def plot_vals(vals, step, ticks, out, prefix):
    i = -1
    v = ticks[0]
    print(prefix, end="")
    while v < ticks[-1]:
        if i == -1:
            if out is not None and v < out[0] and v + step > out[0]:
                print("+", end="")
            elif v > vals[0]:
                print("|", end="")
            else:
                print(" ", end="")
        elif i == 0:
            print("-", end="")
        elif i < 3:
            if v < vals[2] and v + step > vals[2]:  # Median is only one point
                print(":", end="")
            else:
                print("=", end="")
        elif i == 3:
            if v >= vals[4] or v + step > ticks[-1]:
                print("|", end="")
            else:
                print("-", end="")
        if i == 4:
            if out is not None and v < out[1] and v + step > out[1]:
                print("+", end="")
            else:
                print(" ", end="")
        elif v > vals[i + 1]:
            i += 1

        v += step
    print("")


def plot_ticks(ticks, step, width):
    # Plot the axis
    x = ticks[0]
    j = 0
    fmt = "{{:^{}d}}".format(width)
    while x < ticks.max():
        x += step
        if x >= ticks[j]:
            print(fmt.format(ticks[j]), end="")
            j += 1
            x += step * (width - 1)
        else:
            print(" ", end="")

    print("")


def plot_box(data, nticks, outliers, maxima, debug):
    box = [5, 25, 50, 75, 95]
    df = np.array(data, dtype=np.float64)
    vals = np.quantile(df, [x / 100 for x in box])
    out = np.quantile(df, [0.01, 0.99])

    if maxima:
        lo, hi = (df.min(), df.max())
    elif outliers:
        lo, hi = (out[0], out[1])
    else:
        lo, hi = (vals.min(), vals.max())

    ticks = np.unique(np.linspace(lo, math.ceil(hi), nticks, dtype=int))
    # The step is the *width* of a character
    step = (ticks.max() - ticks.min()) / 50

    if debug:
        print(step, vals, ticks, out)

    tick_width = len(str(ticks.max()))

    plot_vals(vals, step, ticks, out if outliers else None, " " * (tick_width // 2))

    plot_ticks(ticks, step, tick_width)


if __name__ == "__main__":
    args = parser.parse_args()

    if args.input is None:
        cin = sys.stdin
    else:
        cin = open(args.input)

    data = read_data(cin)

    plot_box(data, args.nticks, args.outliers, args.maxima, args.debug)
