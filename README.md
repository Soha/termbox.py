# Termbox

Terminal utility to plot the distribution of a (newline-separated) dataset as a boxplot.
A boxplot is a visualisation of a five-number-summary.
For example, using `tips` included &#x2013; from [Seaborn's example](https://seaborn.pydata.org/generated/seaborn.boxplot.html#seaborn.boxplot).

1.  Default boxplot, print all five numbers (5%, 25%, 50%, 75%, 95%). The &rsquo;`:`&rsquo; is the median, &rsquo;`=`&rsquo; the range between the quantiles and &rsquo;`-`&rsquo; the range between the ends of the box, which are &rsquo;`|`&rsquo;.

    ```bash
    $ cat tips | python termbox.py
    |-------=====:============----------------------|
    9          16           24          31           39
    ```

2.  Option to print the *outliers* (1% and 99%).

    ```bash
    $ cat tips | python termbox.py --outliers
    +   |----===:=========---------------|           +
    7          17            28         38           49
    ```

3.  I decided to show the extrema (min and max) of the distribution as ticks rather than in the actual plot. This is due mainly to the resolution problem, the plot becomes unreadable with too much info.

    ```bash
    $ cat tips | python termbox.py --outliers --maxima
        +  |----===:========-------------|         +
    3           15          27           39          51
    ```

4.  You can also change the number of ticks.

    ```bash
    $ cat tips | python termbox.py --outliers --maxima --nticks 7
        +  |----===:========-------------|         +
    3       11      19      27       35      43      51
    ```
    
# Requirements

- `numpy`

# Footnotes

<sup><a id="fn.1" class="footnum" href="#fnr.1">1</a></sup> Distribution of `tips`:

| Key   |      Value |
|:------|-----------:|
| count | 244.000000 |
| mean  |  19.785943 |
| std   |   8.902412 |
| min   |   3.070000 |
| 1%    |      7.250 |
| 25%   |  13.347500 |
| 50%   |  17.795000 |
| 75%   |  24.127500 |
| 99%   |     48.227 |
| max   |  50.810000 |
